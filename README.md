# Shopster App / Java Advanced Features Homework Project

#### About
Small shop-type project incorporating java advanced features: I/O, Collections, Comparable & Comparator, Lambdas. 
Also using Apache CSVCommons for reading from csv file.

#### How to run
- This is console app
- Clone or download repo 
- Import project into IDE
- Run ShopsterApp.java

#### Exercise summary:
1. Read a list of shop items from csv file into List;
2. Calculate price after tax and total price of all items;
3. Implement Comparable for ascending sorting by price;
4. Implement Comparator for sorting in descending order by price (ItemDescendingComparator), then implement additional sorting methods using Comparator (by name  with Lambda, by quantity with anonymous class);
5. Implement bubbleSort method for sorting objects;
6. Print formatted report to the console;
7. Write formatted report into a file;
8. Apply grouping by price using Map, with price as Key and List<Item> as Value;
9. Print Hashmap of grouped objects to console;

