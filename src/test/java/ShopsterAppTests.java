import com.eugenegolobokin.shopster.shop.Item;
import com.eugenegolobokin.shopster.utils.ItemReader;

import java.util.List;

public class ShopsterAppTests {

    public static void importItemsFromFileUsingScannerTest() {
        ItemReader itemReader = new ItemReader();
        List<Item> items = itemReader.importItemsFromFileUsingScanner();

        for (Item item : items) {
            System.out.println(item);
        }
    }

    public static void importItemsFromFileUsingCommonsCsvTest() {
        ItemReader itemReader = new ItemReader();
        List<Item> items = itemReader.importItemsFromFileUsingCommonsCsv();

        for (Item item : items) {
            System.out.println(item);
        }
    }




}
