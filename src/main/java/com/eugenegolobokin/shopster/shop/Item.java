package com.eugenegolobokin.shopster.shop;

public class Item implements Comparable<Item> {

    private String name;
    private double price;
    private int quantity;
    private int tax;
    private double priceWithTax;

    public Item(String name, double price, int quantity, int tax) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.tax = tax;
        this.priceWithTax = calculatePriceWithTax();
    }

    private double calculatePriceWithTax() {
        return quantity * (price + (price * tax / 100));
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getTax() {
        return tax;
    }

    public double getPriceWithTax() {
        return priceWithTax;
    }

    @Override
    public String toString() {
        return "<name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", tax=" + tax +
                '>';
    }


    @Override
    public int compareTo(Item anotherItem) {
        if (this.price == anotherItem.price) {
            return this.name.compareTo(anotherItem.getName());
        }
        return Double.compare(this.price, anotherItem.getPrice());
    }
}
