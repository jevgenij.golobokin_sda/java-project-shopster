package com.eugenegolobokin.shopster.shop;

import com.eugenegolobokin.shopster.utils.ItemDescendingComparator;
import com.eugenegolobokin.shopster.utils.ItemReader;
import com.eugenegolobokin.shopster.utils.ReportWriter;

import java.util.*;

public class ItemManager {

    private List<Item> listOfItems;
    private HashMap<Double, List<Item>> groupedItems;

    public ItemManager() {
        this.listOfItems = new ItemReader().importItemsFromFileUsingCommonsCsv();
    }

    public List<Item> getListOfItems() {
        return listOfItems;
    }

    public void writeReportToFile() {
        ReportWriter reportWriter = new ReportWriter();
        reportWriter.writeReportToFile(listOfItems);
    }

    // Calculation methods

    public int calculateTotalQuantityOfItems() {
        int quantity = 0;
        for (Item item : listOfItems) {
            quantity += item.getQuantity();
        }
        return quantity;
    }

    public double calculateTotalPriceOfItems() {
        double totalPrice = 0;
        for (Item item : listOfItems) {
            totalPrice += item.getPriceWithTax();
        }
        return totalPrice;
    }


    // Sorting methods

    public void sortItemsByPrice() {
        Collections.sort(listOfItems);
    }

    public void sortItemsByPriceInDescendingOrder() {
        ItemDescendingComparator itemDescendingComparator = new ItemDescendingComparator();
        Collections.sort(listOfItems, itemDescendingComparator);
    }

    public void sortItemsByName() {
        Collections.sort(listOfItems, (Item a, Item b) -> a.getName().compareTo(b.getName()));
    }

    public void sortItemsByQuantity() {
        Collections.sort(listOfItems, new Comparator<Item>() {
            @Override
            public int compare(Item item1, Item item2) {
                return item1.getQuantity() - item2.getQuantity();
            }
        });
    }

    public void bubbleSort() {
        boolean swapped;
        for (int i = 0; i < listOfItems.size() - 1; i++) {
            swapped = false;
            for (int j = 0; j < listOfItems.size() - i - 1; j++) {
                if (listOfItems.get(j).getPrice() > listOfItems.get(j + 1).getPrice()) {
                    Item temp = listOfItems.get(j);
                    listOfItems.set(j, listOfItems.get(j + 1));
                    listOfItems.set(j + 1, temp);
                    swapped = true;
                }
            }
            if (!swapped) break;
        }
    }

    // grouping methods

    public void groupItemsByPrice() {
        groupedItems = new HashMap<>();
        for (Item item : listOfItems) {
            Double price = item.getPrice();
            if (!groupedItems.containsKey(price)) {
                List<Item> itemsWithSameKey = new ArrayList<>();
                itemsWithSameKey.add(item);
                groupedItems.put(price, itemsWithSameKey);
            } else {
                groupedItems.get(price).add(item);
            }
        }

    }

    // print methods

    public void printReport() {

        System.out.printf("%-25s %-10s %-10s %-6s %12s\n", "Name", "Price", "Quantity", "% PVM", "Total price");
        System.out.println("-------------------------------------------------------------------");

        for (Item item : listOfItems) {
            System.out.printf("%-25s %-10.2f %-10d %-6d %12.2f\n", item.getName(), item.getPrice(),
                    item.getQuantity(), item.getTax(), item.getPriceWithTax());
        }

        System.out.println("-------------------------------------------------------------------");
        System.out.printf("%25s %-10.2s %-10d %-6s %12.2f\n", "Total:", "",
                calculateTotalQuantityOfItems(), "", calculateTotalPriceOfItems());
    }

    public void printGroupedItemsByPrice() {
        groupedItems.entrySet().forEach(entry -> {
            System.out.println(entry.getKey() + " -> " + entry.getValue());
        });
    }

}
