package com.eugenegolobokin.shopster.utils;

import com.eugenegolobokin.shopster.shop.Item;

import java.util.Comparator;

public class ItemDescendingComparator implements Comparator<Item> {

    @Override
    public int compare(Item firstItem, Item secondItem) {
        if (secondItem.getPrice() < firstItem.getPrice()) return -1;
        if (secondItem.getPrice() > firstItem.getPrice()) return 1;
        else return 0;
    }
}
