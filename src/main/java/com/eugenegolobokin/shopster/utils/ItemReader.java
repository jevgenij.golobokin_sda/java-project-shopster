package com.eugenegolobokin.shopster.utils;

import com.eugenegolobokin.shopster.shop.Item;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ItemReader {

    private static final String ITEM_LIST_FILEPATH = "src/main/resources/itemlist.csv";

    public List<Item> importItemsFromFileUsingCommonsCsv() {

        List<Item> listOfItems = new ArrayList<>();

        try (Reader reader = new FileReader(ITEM_LIST_FILEPATH)) {

            Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(reader);

            for (CSVRecord record : records) {
                String name = record.get("name");
                double price = Double.parseDouble(record.get("price"));
                int quantity = Integer.parseInt(record.get("quantity"));
                int tax = Integer.parseInt(record.get("tax"));

                listOfItems.add(new Item(name, price, quantity, tax));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOfItems;
    }


    /**
     * @deprecated This method is replaced with method which uses Apache Commons CSV library
     * Use {@link #importItemsFromFileUsingCommonsCsv()} instead
     */
    @Deprecated
    public List<Item> importItemsFromFileUsingScanner() {

        List<Item> listOfItems = new ArrayList<>();

        File itemListFile = new File(ITEM_LIST_FILEPATH);

        try {
            Scanner scanner = new Scanner(itemListFile);
            scanner.nextLine();

            while (scanner.hasNext()) {
                String[] valuesFromLine = scanner.nextLine().split(",");

                String name = valuesFromLine[0];
                double price = Double.parseDouble(valuesFromLine[1]);
                int quantity = Integer.parseInt(valuesFromLine[2]);
                int tax = Integer.parseInt(valuesFromLine[3]);

                listOfItems.add(new Item(name, price, quantity, tax));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return listOfItems;
    }

}
