package com.eugenegolobokin.shopster.utils;

import com.eugenegolobokin.shopster.shop.Item;
import com.eugenegolobokin.shopster.shop.ItemManager;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Formatter;
import java.util.List;

public class ReportWriter {

    private static final String REPORT_FILEPATH = "src/main/resources/report.txt";

    public void writeReportToFile(List<Item> listOfItems) {

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(REPORT_FILEPATH, true))) {

            StringBuilder stringBuilder = new StringBuilder();
            Formatter formatter = new Formatter(stringBuilder);

            formatter.format("%-25s %-10s %-10s %-6s %12s\n", "Name", "Price", "Quantity", "% PVM", "Total price");
            stringBuilder.append("-------------------------------------------------------------------\n");

            for (Item item : listOfItems) {
                formatter.format("%-25s %-10.2f %-10d %-6d %12.2f\n", item.getName(), item.getPrice(),
                        item.getQuantity(), item.getTax(), item.getPriceWithTax());
            }

            stringBuilder.append("-------------------------------------------------------------------\n");
            formatter.format("%25s %-10.2s %-10d %-6s %12.2f\n", "Total:", "",
                    new ItemManager().calculateTotalQuantityOfItems(), "", new ItemManager().calculateTotalPriceOfItems());
            stringBuilder.append("\n\n***\n\n");

            bufferedWriter.write(stringBuilder.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
