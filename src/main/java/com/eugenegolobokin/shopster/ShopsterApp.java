package com.eugenegolobokin.shopster;

import com.eugenegolobokin.shopster.shop.ItemManager;

public class ShopsterApp {

    public static void main(String[] args) {

        // reading initial list from file
        System.out.println("\n--------> reading initial list from file\n");
        ItemManager itemManager = new ItemManager();
        itemManager.printReport();
        itemManager.writeReportToFile();
        System.out.println();

        // sorting by price using Comparable interface
        System.out.println("\n--------> sorting by price using Comparable interface\n");
        itemManager.sortItemsByPrice();
        itemManager.printReport();
        itemManager.writeReportToFile();
        System.out.println();

        // sorting by price in descending order using Comparator
        System.out.println("\n--------> sorting by price in descending order using Comparator\n");
        itemManager.sortItemsByPriceInDescendingOrder();
        itemManager.printReport();
        itemManager.writeReportToFile();
        System.out.println();

        // sorting by name - initializing Comparator using lambda
        System.out.println("\n--------> sorting by name - initializing Comparator using lambda\n");
        itemManager.sortItemsByName();
        itemManager.printReport();
        itemManager.writeReportToFile();
        System.out.println();

        // sorting by quantity - initializing Comparator using anonymous implementation
        System.out.println("\n--------> sorting by quantity - initializing Comparator using anonymous implementation\n");
        itemManager.sortItemsByQuantity();
        itemManager.printReport();
        itemManager.writeReportToFile();
        System.out.println();

        // sorting by price using custom Bubble Sort method
        System.out.println("\n--------> sorting by price using custom Bubble Sort method\n");
        itemManager.bubbleSort();
        itemManager.printReport();
        itemManager.writeReportToFile();
        System.out.println();

        // grouping items into HashMap by price
        System.out.println("\n--------> grouping items into HashMap by price\n");
        itemManager.groupItemsByPrice();
        itemManager.printGroupedItemsByPrice();
        itemManager.writeReportToFile();
        System.out.println();

    }


}
